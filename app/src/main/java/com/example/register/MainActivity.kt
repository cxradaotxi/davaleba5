package com.example.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPasswordConfirm: EditText
    private lateinit var button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        registerListeners()
    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmailAddress)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPasswordConfirm = findViewById(R.id.editTextPassword)
        button = findViewById(R.id.button)
    }

    private fun registerListeners(){
        button.setOnClickListener(){

            val email = editTextEmail.text.toString()
            val password = editTextPassword.text.toString()
            val confirmpassword = editTextPasswordConfirm.text.toString()

            if(email.isEmpty()){
                editTextEmail.error = "შეიყვანეთ იმეილი."
                return@setOnClickListener
            }else if(password.isEmpty()){
                editTextPassword.error="შეიყვანეთ პაროლი."
                return@setOnClickListener
            }else if (confirmpassword.isEmpty()){
                editTextPasswordConfirm.error ="გთხოვთ,გაიმეორეთ პაროლი."
                return@setOnClickListener
            }else if (!email.contains("@")){
                editTextEmail.error = "გთხოვთ, სწორად შეიყვანოთ მეილი."
                return@setOnClickListener
            }else if (password.length<9){
                editTextPassword.error="პაროლი უნდა შეიცავდეს 8-ზე მეტ სიმბოლოს."
                return@setOnClickListener
            }else if(!password.contains(
                    Regex("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!\\-_?&])"))){
                editTextPassword.error = "თქვენი პაროლი უნდა შეიცავდეს დიდ და პატარა ასოებს, სიმბოლოებს და ციფრებს."
                return@setOnClickListener
            }else if(password!=confirmpassword){
                editTextPasswordConfirm.error="შეყვანილი პაროლები არ ემთხვევა ერთმანეთს."
                return@setOnClickListener
            }else Toast.makeText(this, "რეგისტრაცია წარმატებით დასრულდა.", Toast.LENGTH_SHORT).show()

            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email,password)
        }

    }


}
